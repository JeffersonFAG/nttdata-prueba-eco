import axios from "axios";

export const getItems = async (URL) => {
  const res = await axios.get(URL);
  return res.data;
};
