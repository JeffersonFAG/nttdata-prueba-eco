function searchingTerm(term) {
  return function (dato) {
    return (
      dato.id.includes(term) ||
      dato.title.toString().toLowerCase().includes(term.toLowerCase()) ||
      !term
    );
  };
}
export default searchingTerm;
