// Import the functions you need from the SDKs you need
import firebase from "firebase";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCxBDbbAW9MpWr9iCSGoknM7DKz0XhTLBk",
  authDomain: "eco-p-f3fd4.firebaseapp.com",
  projectId: "eco-p-f3fd4",
  storageBucket: "eco-p-f3fd4.appspot.com",
  messagingSenderId: "408479332640",
  appId: "1:408479332640:web:07893ea26a6bde16d27f45",
};

// Initialize Firebase
const firebaseAPP = firebase.initializeApp(firebaseConfig);
export const db = firebaseAPP.firestore();
