import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CreateItem from "./components/CreateItem";
import ViewTodos from "./components/ViewTodos";
import ViewList from "./components/ViewList";
import { useEffect } from "react";
import { db } from "./firebase/firebase";
import { useDispatch } from "react-redux";
import { saveItems } from "./feature/items/itemSlice";
import NoFound from "./components/NoFound";

function App() {
  const dispatch = useDispatch();

  //Listener FireBase
  useEffect(() => {
    db.collection("post")
      .get()
      .then((querySnapshot) => {
        dispatch(
          saveItems(
            querySnapshot.docs.map((doc) => ({
              id: doc.data().itemId,
              title: doc.data().title,
            }))
          )
        );
      });
  }, [dispatch]);
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<ViewTodos />} />
        <Route path="/create-item" element={<CreateItem />} />
        <Route path="/edit-item/:id" element={<CreateItem />} />
        <Route path="/view-item" element={<ViewList />} />
        <Route path="/*" element={<NoFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
