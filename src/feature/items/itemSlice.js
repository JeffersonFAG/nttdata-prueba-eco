import { createSlice } from "@reduxjs/toolkit";

//initial State
const initialState = [];

//Slice for Items
export const itemsSlice = createSlice({
  name: "items",
  initialState,
  reducers: {
    //create item
    addItem: (state, action) => {
      state.push(action.payload);
    },

    //edit item
    editItem: (state, action) => {
      console.log(action.payload);
      const { id, title } = action.payload;
      const foundItem = state.find((item) => item.id === id);
      if (foundItem) {
        foundItem.id = id;
        foundItem.title = title;
      }
    },

    //delete item
    deleteItem: (state, action) => {
      const itemFound = state.find((item) => item.id === action.payload);

      if (itemFound) {
        state.splice(state.indexOf(itemFound), 1);
      }
    },

    //listener firebase
    saveItems: (state, action) => action.payload,
  },
});

//export reducers
export default itemsSlice.reducer;

//export actions
export const { addItem, deleteItem, editItem, saveItems } = itemsSlice.actions;
