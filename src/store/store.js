import { configureStore } from "@reduxjs/toolkit";
import itemsReducer from "../feature/items/itemSlice";
import itemsApiReducer from "../feature/itemsApi/apiSlice";

export const store = configureStore({
  reducer: {
    items: itemsReducer,
    apiItems: itemsApiReducer,
  },
});
