const SearchBar = ({ onChange }) => {
  return (
    <input
      style={{ padding: "5px" }}
      name="term"
      placeholder="Busqueda por ID o TITLE"
      onChange={onChange}
    />
  );
};
export default SearchBar;
