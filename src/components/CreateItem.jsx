import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { addItem, editItem } from "../feature/items/itemSlice";
import { db } from "../firebase/firebase";
import BtnBack from "./BtnBack";

const CreateItem = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();
  const items = useSelector((state) => state.items);

  //initial state Form
  const [form, setForm] = useState({
    title: "",
    id: "",
  });

  //Change value form
  function onChange(e) {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  }

  //Submit Form
  function onSubmit(e) {
    e.preventDefault();
    if (!form.title || !form.id) {
      alert("DATOS INCOMPLETOS");
      return;
    }
    if (id) {
      dispatch(editItem({ ...form, id: id }));
    } else {
      //Config redux
      dispatch(addItem(form));
      alert("Se agrego un nuevo Item");

      //Config firebase
      db.collection("post").add({
        title: form.title,
        itemId: form.id,
        dataTime: Date.now(),
      });
    }
    navigate("/");
  }

  useEffect(() => {
    if (id) {
      setForm(items.find((item) => item.id === id));
    }
  }, [id, items]);

  return (
    <form onSubmit={onSubmit}>
      <h2>
        {id ? "Edita Item" : "Crear Item"} <BtnBack />
      </h2>
      <input
        type="text"
        name="title"
        placeholder="Title"
        onChange={onChange}
        value={form.title}
      />
      <input
        type="text"
        name="id"
        placeholder="Id"
        onChange={onChange}
        value={form.id}
      />
      <button type="submut">Guardar</button>
    </form>
  );
};

export default CreateItem;
