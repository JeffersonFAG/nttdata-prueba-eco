import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { deleteItem } from "../feature/items/itemSlice";
import searchingTerm from "../services/search";
import NavBar from "./NavBar";
import SearchBar from "./SearchBar";

const ViewTodos = () => {
  const itemsState = useSelector((state) => state.items);
  const dispatch = useDispatch();

  //delete Item
  function deleteItemList(id) {
    dispatch(deleteItem(id));
  }

  //Config Search
  const [data, setData] = useState([]);
  const [term, setTerm] = useState("");

  function onChangeSearch(e) {
    setTerm(e.target.value);
  }

  useEffect(() => {
    setData(itemsState);
  }, [itemsState]);

  return (
    <div>
      <header>
        <h1>Prueba NttData</h1>
        <NavBar />
        <hr />
      </header>
      <nav
        style={{ display: "flex", gap: "1rem", margin: "1rem", padding: "0px" }}
      >
        Listado de Items
        <SearchBar onChange={onChangeSearch} />
      </nav>
      <hr />
      {data.length > 0 ? (
        data.filter(searchingTerm(term)).map((item) => (
          <nav style={{ margin: "1rem", width: "50%" }} key={item.id}>
            <h3>{item.title}</h3>
            <h3>{item.id}</h3>
            <button onClick={() => deleteItemList(item.id)}>Delete</button>
            <Link to={`/edit-item/${item.id}`}>
              <button>Edit</button>
            </Link>
            <hr />
          </nav>
        ))
      ) : (
        <h2>Loading...</h2>
      )}
    </div>
  );
};

export default ViewTodos;
