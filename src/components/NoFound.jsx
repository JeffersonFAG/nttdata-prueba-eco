const NoFound = () => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "0px",
        textAlign: "center",
      }}
    >
      <h1>Pag No Found</h1>
      <h2>Error! 404</h2>
    </div>
  );
};

export default NoFound;
