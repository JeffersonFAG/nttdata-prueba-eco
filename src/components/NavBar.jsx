import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <nav style={{ display: "flex", gap: "1rem", margin: "1rem" }}>
      <Link to="view-item">
        <button>View Items Api</button>
      </Link>
      <Link to="create-item">
        <button>Crear Item</button>
      </Link>
    </nav>
  );
};

export default NavBar;
