import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { noItems, viewItems } from "../feature/itemsApi/apiSlice";
import { getItems } from "../services";
import BtnBack from "./BtnBack";
import SearchBar from "./SearchBar";

const ViewList = () => {
  const dispatch = useDispatch();
  const apiItems = useSelector((state) => state.apiItems);

  const URL = "https://jsonplaceholder.typicode.com/todos";
  //Update items
  useEffect(() => {
    getItems(URL).then((res) => {
      if (!res.err) {
        dispatch(viewItems(res));
      } else {
        dispatch(noItems());
      }
    });
  }, [dispatch]);

  //Config Search
  const [data, setData] = useState([]);
  const [term, setTerm] = useState("");

  useEffect(() => {
    setData(apiItems);
  }, [apiItems]);

  function onChangeSearch(e) {
    setTerm(e.target.value);
  }

  return (
    <div>
      <h3>
        Listado de usuarios{"  "}
        <BtnBack />
      </h3>
      <div style={{ margin: "1rem" }}>
        <SearchBar onChange={onChangeSearch} />
      </div>
      <table>
        <thead>
          <tr>
            <th>TITLE</th>
            <th>ID</th>
          </tr>
        </thead>
        <tbody>
          {data.length > 0 ? (
            data.map((item) => (
              <tr key={item.id}>
                <td>{item.title}</td>
                <td>{item.id}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="4">
                <h2>Loading...</h2>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default ViewList;
