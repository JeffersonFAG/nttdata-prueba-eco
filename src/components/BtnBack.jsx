import { useNavigate } from "react-router-dom";

const BtnBack = () => {
  const navigate = useNavigate();
  return <button onClick={() => navigate("/")}>Back</button>;
};
export default BtnBack;
